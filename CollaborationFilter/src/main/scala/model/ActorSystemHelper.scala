package model

import akka.actor.{ActorRef, ActorSystem}

/**
  * Created by chenjianwen on 2016/3/15.
  */
class ActorSystemHelper {

}

object ActorSystemHelper{
  private var actorSystem:ActorSystem = _

  def getActorSystem:ActorSystem={
    if(actorSystem != null) actorSystem
    actorSystem = ActorSystem.create("inserRedisActor")
    actorSystem
  }
}
